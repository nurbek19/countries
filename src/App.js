import React, {Component} from 'react';
import CountryName from './components/CountryName/CountryName';
import CountryInformation from './components/CountryInformation/CountryInformation';
import './App.css';

class App extends Component {
    state = {
        countries: [],
        country: {},
        borders: []
    };

    _requestURL(url) {
        return fetch(url).then(
            response => {
                if (response.ok) {
                    return response.json();
                }

                throw new Error('Something went wrong');
            }
        )
    }

    showInformation = index => {
        const country = this.state.countries[index];

        this._requestURL('https://restcountries.eu/rest/v2/alpha/' + country.alpha3Code).then(country => {
            this.setState({country});
            return Promise.all(country.borders.map(border => {
                return this._requestURL('https://restcountries.eu/rest/v2/alpha/' + border).then(country => {
                    return {name: country.name};
                })
            }));
        }).then(borders => {
            this.setState({borders});
        }).catch(error => {
            console.log(error);
        });

    };

    componentDidMount() {
        this._requestURL('https://restcountries.eu/rest/v2/all?fields=name;alpha3Code').then(
            countries => {
                this.setState({countries});
            }
        );
    }

    render() {

        let countryInformation = <p>Choose country</p>;

        if (Object.keys(this.state.country).length !== 0) {
            countryInformation = <CountryInformation
                name={this.state.country.name}
                img={this.state.country.flag}
                capital={this.state.country.capital}
                population={this.state.country.population}
                borders={this.state.borders}
            />
        }

        return (
            <div className="App">
                <ul className="countries">
                    {this.state.countries.map((country, index) => {
                        return <CountryName
                            name={country.name}
                            key={index}
                            getInformation={() => this.showInformation(index)}
                        />
                    })}
                </ul>

                <div className="about">
                    {countryInformation}
                </div>
            </div>
        );
    }
}

export default App;
