import React from 'react';

const CountryName = props => {
  return (
      <li onClick={props.getInformation}>{props.name}</li>
  )
};

export default CountryName;