import React from 'react';

const CountryInformation = props => {
    return (
        <div className="country">
            <h4>{props.name}</h4>
            <img src={props.img} alt="flag"/>
            <p><b>Capital: </b> {props.capital}</p>
            <p><b>Population: </b> {props.population}</p>

            <p><b>Borders with:</b></p>
            <ul>{props.borders.map((country, index) => <li key={index}>{country.name}</li>)}</ul>
        </div>
    )
};

export default CountryInformation;